@extends('layout_back')

@section('title')
<h1>
    Journee du {{ (new DateTime($journee->date))->format('d/m/Y') }}
</h1>
@stop

@section('content')


<!-- Main content -->
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <!-- /.box-header -->
                <div class="box-body">


                    <h3>Liste des rencontres</h3>

                    <table class="table table-bordered">
                        <thead class="thead-inverse">
                            <tr>
                                <th>Equipe</th>
                                <th>Division</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($rencontres as $rencontre)
                            <tr>
                                <td>
                                    {{ $rencontre->equipe->libelle}}
                                </td>
                                <td>
                                    {{ $rencontre->equipe->division }}
                                </td>
                                <td class="col-md-1">
                                    {!! Form::open(['route' => ["journee.destroy", $journee->id], 'method' => 'delete', 'id' => "form".$journee->id]) !!}
                                        <button type="submit" id="{{ $journee->id }}" class="btn btn-danger btn-circle jsDeleteButton"><i class="fa fa-times"></i></button>
                                    {!! Form::close() !!}
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <h3>Disponibilités</h3>
                    <table class="table table-bordered">
                        <thead class="thead-inverse">
                            <tr>
                                <th>Joueur</th>
                                @foreach ($equipes as $equipe)
                                <th>{{ $equipe->libelle }}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($equipes as $equipe)
                            <tr>
                                <td>{{ $equipe->libelle }}</td>   
                                <td></td>
                                <td></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@stop
