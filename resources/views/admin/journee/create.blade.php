@extends('layout_back')

@section('title')
<h1>
    Créer une journée
    <small>- Page de création d'une journée</small>
</h1>
@stop

@section('content')
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<!-- Main content -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            {!! Form::open(['route' => "journee.store", 'method' => 'post']) !!}
                <div class="box-header">
                    <h3 class="box-title">  </h3>

                    <div class="form-group">
                        <label for="date">Date de la journée :  </label>
                        <input class="form-control" placeholder="Date :" name="date" type="date"> 
                    </div>
                </div>

                <!-- /.box-header -->
                
                <button type="submit" class="btn btn-success btn-lg btn-block">Créer</button>

            {!! Form::close() !!}
        </div>
        <!-- /.box -->
    </div>
</div>

@stop
