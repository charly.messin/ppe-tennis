@extends('template')

@section("tittle")
Documents
@stop
@section("sous-menu")
@stop
@section("content")


  <!-- Main content -->
<div class="text">
<table>
    <thead class="panel-body">
        <tr>
        <th style="text-align:center">Titre</th>
        <th style="text-align:center">Description</th>
        <th style="text-align:center">Fichier</th>

        </tr>

    </thead>
    <tbody>

        @foreach ($tab_docs as $unDoc)
            <tr>
                <td class="col-md-1 col-md-offset-4 ">
                    {{ $unDoc->titre }}
                </td >
                <td class="col-md-1 col-md-offset-4">
                    {{ $unDoc->description }}
                </td>
                <td class="col-md-5" >
                    <center><objet  data="{{ url('doc/') ."/". $unDoc["contenu"] }}" alt="img{{ $unDoc->id }}" ><a href="{{ url('doc/') ."/". $unDoc["contenu"] }}" target="_blank" >{{ $unDoc->contenu }}</a> </objet></center>
                </td>    
            </tr>
        @endforeach
    </tbody>
</table>
</div>
@endsection
