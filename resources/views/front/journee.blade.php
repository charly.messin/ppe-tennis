@extends('template')

@section('title')
<h1>
    Liste des journées
</h1>
@stop

@section('content')


<!-- Main content -->
<div class="container">
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <!-- /.box-header -->
                <div class="box-body">
                    
                    <!-- search form (Optional) -->
                    <form action="#" method="get">
                        <div class="input-group margin">
                            <input type="text" name="q" class="form-control" placeholder="Rechercher . . .">
                            <div class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-info btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>

                    <table class="table table-bordered">
                        <thead class="thead-inverse">
                            <tr>
                                <th>Nb</th>
                                <th>Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($journees as $journee)
                            <tr>
                                <td class="col-md-1">
                                    {{ $journee->id }}
                                </td>
                                <td class="col-md-4">
                                    {{ (new DateTime($journee->date))->format('d/m/Y') }}
                                </td>
                                <td>
                                    <button class="btn btn-success">voir les joueurs</button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody> 
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

@stop