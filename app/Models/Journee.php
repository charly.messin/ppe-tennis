<?php

namespace App\Models;
use Models\Rencontre;
use Models\User;
use Illuminate\Database\Eloquent\Model;

class Journee extends Model
{
    public function rencontres()
    {
        return $this->hasMany('App\Models\Rencontre');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }
}
