<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Models\Journee;
use App\Models\Equipe;
use App\Models\Rencontre;
use Illuminate\Http\Request;


class JourneeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $journees = Journee::all();
        return view('admin.journee.index', compact('journees'));
    }

    public function indexFront()
    {
        $journees = Journee::all();
        //dd($journees);
        return view('front.journee', compact('journees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.journee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Validator $validator)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect(route('journee.create'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $journee = new Journee();
        $journee->date = $request->get('date');
        $journee->save();
        $request->session()->flash('success', 'La journée à été ajoutée !');

        $equipes = Equipe::all();
        foreach ($equipes as $equipe)
        {
            $rencontre = new Rencontre();
            $rencontre->dte = $journee->date;
            $rencontre->equipe_id = $equipe->id;
            $rencontre->journee_id = $journee->id;
            $rencontre->save();
        }

        return redirect()->route("journee.index");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $journee = Journee::find($id);
        $rencontres = $journee->rencontres;
        $equipes = Equipe::all();
        return view('admin.journee.show', compact('journee', 'rencontres', 'equipes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
