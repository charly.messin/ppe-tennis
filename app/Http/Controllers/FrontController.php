<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Document;

class FrontController extends Controller
{

  public function liens_utiles()
   {
       return view('front.liens_utiles');
   }

   public function contact()
   {
       return view('front.contact');
   }

   public function galerie()
   {
       return view('front.galerie');
   }

   public function document()
   {
       $documents = Document::all();
       return view('front.document')->with('tab_docs', $documents);
   }

}
