<?php

namespace App\Http\Controllers;
use App\Models\Document;
use Illuminate\Http\Request;
use Validator;
use Image;
Use File;
use Auth;

class DocumentController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {


    $lesDocs = Document::all();


      return view('admin.document.index')
      ->with('tab_docs',$lesDocs);
  }


  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response */

  public function create()
  {
    //$user = Document::all();

    //dd($user);
    return view('admin.document.create');
  }



  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request, Validator $validator)
  {
    $validator = Validator::make($request->all(), [
      'titre' => 'required',
      'description' => 'required',
      'document' => 'mimes:pdf|max:1024',
    ]);
    if ($validator->fails()){
      return redirect()->route('document.create')->withErrors($validator)->withInput();
    }

    $document = new Document();

    $document->titre = $request->get('titre');

    $document->description = $request->get('description');

    $document->user_id = auth::user()->id;

    $contenu = $request->file('document');

    $document->type_document = $contenu->getClientOriginalName();

    $documentname = time().'.'.$contenu->getClientOriginalName();

    $destinationPath = public_path('doc/');

    $contenu->move($destinationPath, $documentname);

    $document->contenu = $documentname;

    $document->save();

    $request->session()->flash('success', 'Le fichier à été Ajouté !');

    return redirect()->route("document.index");
    
  }




  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($user_id)
  {
    $tab_docs = Document::find($user_id);


    return view('site.document.show', compact('tab_docs'));
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //
  }


  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    //
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy(Request $request, $id)
  {
    $doc = Document::find($id);

    File::delete("doc/" . $doc->contenu);

    $doc->delete();

    $request->session()->flash('success', 'Le doc à été Supprimé !');

    return redirect()->route("document.index");
  }

  public function adestroy(Request $request, $id)
  {
    $doc = Document::find($id);

    File::delete("doc/" . $doc->fichier);

    $doc->delete();

    $request->session()->flash('success', 'Le doc à été Supprimé !');

    return redirect()->route("document.home");
  }
}
