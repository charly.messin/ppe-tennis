<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserJourneeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_journee', function (Blueprint $table){
            $table->increments('id');
            $table->enum('disponibilite', array('oui', 'non'))->default('non');
            $table->integer('user_id');
            $table->integer('journee_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('journee_id')->references('id')->on('journees');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users_journee');
    }
}
