<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCleJourneeRencontre extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rencontres', Function(Blueprint $table){
            $table->integer('journee_id')->unsigned();
            $table->foreign('journee_id')->references('id')->on('journees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('FK_journee_id_rencontres');
    }
}
